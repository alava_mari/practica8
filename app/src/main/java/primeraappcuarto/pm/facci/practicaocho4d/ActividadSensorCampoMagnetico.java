package primeraappcuarto.pm.facci.practicaocho4d;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadSensorCampoMagnetico extends AppCompatActivity implements SensorEventListener {

    TextView texto;
    SensorManager sensorManager;
    private Sensor magnetic_field;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_sensor_campo_magnetico);
        texto = (TextView) findViewById(R.id.lblTexto);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        magnetic_field = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if(magnetic_field == null) {
            Toast.makeText(this, "Este sensr no está disponible en este dispositivo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float x,y,z;
        x = sensorEvent.values[0];
        y = sensorEvent.values[1];
        z = sensorEvent.values[2];
        texto.setText("");
        texto.append("\n El valor de x: " + x + "\n El valor de y: " + y + "\n El valor de z: " + z);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, magnetic_field, sensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}
