package primeraappcuarto.pm.facci.practicaocho4d;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ActividadSensores extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_sensores);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.sensor1:
                intent = new Intent(ActividadSensores.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.sensor2:
                intent = new Intent(ActividadSensores.this, ActividadSensorProximidad.class);
                startActivity(intent);
                break;
            case R.id.sensor3:
                intent = new Intent(ActividadSensores.this, ActividadSensorCampoMagnetico.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}
