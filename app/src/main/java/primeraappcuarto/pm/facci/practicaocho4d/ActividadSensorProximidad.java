package primeraappcuarto.pm.facci.practicaocho4d;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadSensorProximidad extends AppCompatActivity implements SensorEventListener{

    TextView texto;
    SensorManager sensorManager;
    private Sensor proximidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_sensor_proximidad);
        texto = (TextView) findViewById(R.id.lblTexto);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if(proximidad == null) {
            Toast.makeText(this, "Este sensr no está disponible en este dispositivo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.values[0] < proximidad.getMaximumRange()) {
            Toast.makeText(this, "Hay algo cerca", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "No hay nada cerca", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, proximidad, 2 * 1000 * 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }


}
